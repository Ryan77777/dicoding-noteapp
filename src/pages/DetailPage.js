import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import NoteDetailTitle from "../components/NoteDetailTitle";
import NoteDetailDate from "../components/NoteDetailDate";
import NoteDetailBody from "../components/NoteDetailBody";
import ArchiveButton from "../components/ArchiveButton";
import DeleteButton from "../components/DeleteButton";
import UnarchiveButton from "../components/UnarchiveButton";

// API
import { archiveNote, deleteNote, getNote, unarchiveNote } from "../utils/api";

function DetailPage() {
    const { id } = useParams();
    const navigate = useNavigate();

    const [data, setData] = useState(null);

    const onUnarchiveHandler = () => {
        unarchiveNote(id);
        navigate("/");
    };

    const onArchiveHandler = () => {
        archiveNote(id);
        navigate("/");
    };

    const onDeleteHandler = () => {
        deleteNote(id);
        navigate("/");
    };

    React.useEffect(() => {
        getNote(id).then(({ data }) => {
            setData(data);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return data ? (
        <section className="detail-page">
            <NoteDetailTitle title={data?.title} />
            <NoteDetailDate createdAt={data?.createdAt} />
            <NoteDetailBody body={data?.body} />

            <div className="detail-page__action">
                {data?.archived ? (
                    <UnarchiveButton onUnarchive={onUnarchiveHandler} />
                ) : (
                    <ArchiveButton onArchive={onArchiveHandler} />
                )}
                <DeleteButton onDelete={onDeleteHandler} />
            </div>
        </section>
    ) : null;
}

export default DetailPage;
