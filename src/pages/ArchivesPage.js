import React, { useContext, useState } from "react";
import { useSearchParams } from "react-router-dom";

import NoteList from "../components/NoteList";
import NoteListEmpty from "../components/NoteListEmpty";
import SearchBar from "../components/SearchBar";
import LocaleContext from "../contexts/LocaleContext";
import { getArchivedNotes } from "../utils/api";

const ArchivesPage = () => {
    const { locale } = useContext(LocaleContext);
    const [searchParams, setSearchParams] = useSearchParams();
    const [notes, setNotes] = useState([]);
    const [keyword, setKeyword] = React.useState(() => {
        return searchParams.get("keyword") || "";
    });

    const onKeywordChangeHandler = (keyword) => {
        setKeyword(keyword);
        setSearchParams({ keyword });
    };

    React.useEffect(() => {
        getArchivedNotes().then(({ data }) => {
            setNotes(data);
        });
    }, []);

    const filteredData = notes?.filter((obj) => {
        return obj.title.toLowerCase().includes(keyword.toLowerCase());
    });

    return (
        <section className="homepage">
            <h2>{locale === "id" ? "Catatan Aktif" : "Archived Note"}</h2>
            <SearchBar
                keyword={keyword}
                keywordChange={onKeywordChangeHandler}
            />
            {filteredData?.length > 0 ? (
                <NoteList notes={filteredData} />
            ) : (
                <NoteListEmpty />
            )}
        </section>
    );
};

export default ArchivesPage;
