import React, { useContext, useState } from "react";
import { useSearchParams } from "react-router-dom";

import NoteList from "../components/NoteList";
import NoteListEmpty from "../components/NoteListEmpty";
import SearchBar from "../components/SearchBar";
import AddButton from "../components/AddButton";

// API
import { getActiveNotes } from "../utils/api";
import LocaleContext from "../contexts/LocaleContext";

const HomePage = () => {
    const { locale } = useContext(LocaleContext);
    const [searchParams, setSearchParams] = useSearchParams();
    const [notes, setNotes] = useState([]);
    const [keyword, setKeyword] = React.useState(() => {
        return searchParams.get("keyword") || "";
    });

    React.useEffect(() => {
        getActiveNotes().then(({ data }) => {
            setNotes(data);
        });
    }, []);

    const onKeywordChangeHandler = (keyword) => {
        setKeyword(keyword);
        setSearchParams({ keyword });
    };

    const filteredData = notes?.filter((obj) => {
        return obj.title.toLowerCase().includes(keyword.toLowerCase());
    });

    return (
        <section className="homepage">
            <h2>{locale === "id" ? "Catatan Aktif" : "Active Note"}</h2>
            <SearchBar
                keyword={keyword}
                keywordChange={onKeywordChangeHandler}
            />
            {filteredData.length > 0 ? (
                <NoteList notes={filteredData} />
            ) : (
                <NoteListEmpty />
            )}

            <AddButton />
        </section>
    );
};

export default HomePage;
