import React, { useState } from "react";

import SubmitButton from "../components/SubmitButton";

import NoteForm from "../components/NoteForm";
import { addNote } from "../utils/api";
import { useNavigate } from "react-router-dom";

const AddPage = () => {
    const navigate = useNavigate();
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");

    const onTitleChangeEventHandler = (event) => {
        setTitle(event.target.value.slice(0, 50));
    };
    const onBodyChangeEventHandler = (event) => {
        setBody(event.target.innerHTML);
    };

    const onSubmitEventHandler = (event) => {
        event.preventDefault();
        addNote({ title, body });
        navigate("/");
    };

    return (
        <section className="add-new-page">
            <NoteForm
                title={title}
                body={body}
                onTitleChangeEventHandler={onTitleChangeEventHandler}
                onBodyChangeEventHandler={onBodyChangeEventHandler}
            />
            <SubmitButton onSubmit={onSubmitEventHandler} />
        </section>
    );
};

export default AddPage;
