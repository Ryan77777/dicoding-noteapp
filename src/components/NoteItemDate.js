import React from "react";
import { showFormattedDate } from "../utils";
import PropTypes from "prop-types";

function NoteItemDate({ createdAt }) {
    return (
        <p className="note-item__createdAt">{showFormattedDate(createdAt)}</p>
    );
}

NoteItemDate.propTypes =  {
    createdAt: PropTypes.string.isRequired,
};

export default NoteItemDate;
