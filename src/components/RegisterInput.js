import React, { useState } from "react";
import PropTypes from "prop-types";

const RegisterInput = ({ register }) => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirm, setConfirm] = useState("");

    const onNameChange = (event) => {
        setName(event.target.value);
    };

    const onEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const onPasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const onConfirmPasswordChange = (event) => {
        setConfirm(event.target.value);
    };

    const onSubmitHandler = (event) => {
        event.preventDefault();

        if (password !== confirm) {
            alert("Password and password confirm must be same.");
        } else {
            register({
                name,
                email,
                password,
                confirm,
            });
        }
    };

    return (
        <form onSubmit={onSubmitHandler} className="input-register">
            <label htmlFor="name">Name</label>
            <input type="text" value={name} onChange={onNameChange} />
            <label htmlFor="email">Email</label>
            <input type="email" value={email} onChange={onEmailChange} />
            <label htmlFor="current-password">Password</label>
            <input
                type="password"
                autoComplete="current-password"
                value={password}
                onChange={onPasswordChange}
            />
            <label htmlFor="confirm-password">Confirm Password</label>
            <input
                type="password"
                autoComplete="current-password"
                value={confirm}
                onChange={onConfirmPasswordChange}
            />
            <button>Register</button>
        </form>
    );
};

RegisterInput.propTypes = {
    register: PropTypes.func.isRequired,
};

export default RegisterInput;
