import React from "react";
import PropTypes from "prop-types";

function NoteForm({
    title,
    onTitleChangeEventHandler,
    onBodyChangeEventHandler,
}) {
    return (
        <div className="add-new-page__input">
            <input
                className="add-new-page__input__title"
                placeholder="Catatan rahasia"
                value={title}
                onChange={onTitleChangeEventHandler}
            />
            <div
                className="add-new-page__input__body"
                contentEditable="true"
                data-placeholder="Sebenarnya saya adalah ...."
                onInput={onBodyChangeEventHandler}
            ></div>
        </div>
    );
}

NoteForm.propTypes =  {
    title: PropTypes.string.isRequired,
    onTitleChangeEventHandler: PropTypes.func.isRequired,
    onBodyChangeEventHandler: PropTypes.func.isRequired,
};

export default NoteForm;
