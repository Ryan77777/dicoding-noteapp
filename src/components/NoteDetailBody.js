import React from "react";
import PropTypes from "prop-types";

function NoteDetailBody({ body }) {
    return <p className="detail-page__body">{body}</p>;
}

NoteDetailBody.propTypes =  {
    body: PropTypes.string.isRequired,
};

export default NoteDetailBody;
