import React, { useState } from "react";
import PropTypes from "prop-types";

const LoginInput = ({ login }) => {
    const [email, setEmail] = useState("");
    const [password, setPasswod] = useState("");

    const onEmailChangeHandler = (event) => {
        setEmail(event.target.value);
    };

    const onPasswordChangeHandler = (event) => {
        setPasswod(event.target.value);
    };

    const onSubmitHandler = (event) => {
        event.preventDefault();
        login({
            email,
            password,
        });
    };

    return (
        <form onSubmit={onSubmitHandler} className="input-login">
            <label htmlFor="email">Email</label>
            <input type="email" autoComplete="username" value={email} onChange={onEmailChangeHandler} />
            <label htmlFor="password">Password</label>
            <input
                type="password"
                autoComplete="current-password"
                value={password}
                onChange={onPasswordChangeHandler}
            />
            <button>Login</button>
        </form>
    );
};

LoginInput.propTypes = {
    login: PropTypes.func.isRequired,
};

export default LoginInput;
