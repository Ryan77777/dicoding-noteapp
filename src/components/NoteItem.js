import React from "react";
import PropTypes from "prop-types";
import NoteItemTitle from "./NoteItemTitle";
import NoteItemDate from "./NoteItemDate";
import NoteItemBody from "./NoteItemBody";

function NoteItem({id, title, createdAt, body }) {
    return (
        <article className="note-item">
            <NoteItemTitle id={id} title={title} />
            <NoteItemDate createdAt={createdAt} />
            <NoteItemBody body={body} />
        </article>
    );
}

NoteItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
};

export default NoteItem;
