import React from "react";

function ArchiveListEmpty() {
    return (
        <section className="notes-list-empty">
            <p className="notes-list__empty">Arsip kosong</p>
        </section>
    );
}

export default ArchiveListEmpty;
