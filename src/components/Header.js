import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { FiLogOut } from "react-icons/fi";
import ToggleTheme from "./ToggleTheme";
import ToggleLocale from "./ToggleLocale";
import LocaleContext from "../contexts/LocaleContext";

function Header({ logout, name }) {
    const { locale } = React.useContext(LocaleContext);
    return (
        <header>
            <h1>
                <Link to="/">
                    {locale === "id" ? "Aplikasi Catatan" : "Notes App"}
                </Link>
            </h1>
            {name && (
                <nav className="navigation">
                    <ul>
                        <li>
                            <Link to="/archives">
                                {locale === "id" ? "Terarsip" : "Archived"}
                            </Link>
                        </li>
                    </ul>
                </nav>
            )}
            <ToggleLocale />
            <ToggleTheme />
            {name && (
                <button className="button-logout" onClick={logout}>
                    <FiLogOut />
                    {name}
                </button>
            )}
        </header>
    );
}

Header.propTypes = {
    logout: PropTypes.func,
    name: PropTypes.string,
};

export default Header;
