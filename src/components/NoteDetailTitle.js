import React from "react";
import PropTypes from "prop-types";

function NoteDetailTitle({ title }) {
    return <h3 className="detail-page__title">{title}</h3>;
}

NoteDetailTitle.propTypes =  {
    title: PropTypes.string.isRequired,
};

export default NoteDetailTitle;
