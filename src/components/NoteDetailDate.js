import React from "react";
import { showFormattedDate } from "../utils";
import PropTypes from "prop-types";

function NoteDetailDate({ createdAt }) {
    return (
        <p className="detail-page__createdAt">{showFormattedDate(createdAt)}</p>
    );
}

NoteDetailDate.propTypes =  {
    createdAt: PropTypes.string.isRequired,
};

export default NoteDetailDate;
