import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";

// Components
import Header from "./components/Header";

// Pages
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import HomePage from "./pages/HomePage";
import ArchivesPage from "./pages/ArchivesPage";
import DetailPage from "./pages/DetailPage";
import AddPage from "./pages/AddPage";
import PageNotFound from "./pages/PageNotFound";

// API
import { getUserLogged, putAccessToken } from "./utils/api";
import { ThemeProvider } from "./contexts/ThemeContext";
import { LocaleProvider } from "./contexts/LocaleContext";

function App() {
    const [authedUser, setAuthedUser] = useState(null);
    const [initializing, setInitializing] = useState(true);

    const [theme, setTheme] = React.useState(
        localStorage.getItem("theme") || "light"
    );

    const [locale, setLocale] = React.useState(
        localStorage.getItem("locale") || "id"
    );

    const toggleTheme = () => {
        setTheme((prevState) => {
            const newTheme = prevState === "light" ? "dark" : "light";
            localStorage.setItem("theme", newTheme);

            return newTheme;
        });
    };

    const toggleLocale = () => {
        setLocale((prevState) => {
            const newLocale = prevState === "id" ? "en" : "id";
            localStorage.setItem("locale", newLocale);

            return newLocale;
        });
    };

    const themeContextValue = React.useMemo(() => {
        return {
            theme,
            toggleTheme,
        };
    }, [theme]);

    const localeContextValue = React.useMemo(() => {
        return {
            locale,
            toggleLocale,
        };
    }, [locale]);

    React.useEffect(() => {
        const getUser = async () => {
            const { data } = await getUserLogged();
            setAuthedUser(data);
            setInitializing(false);
        };
        getUser();
    }, []);

    const onLoginSuccess = async ({ accessToken }) => {
        putAccessToken(accessToken);
        const { data } = await getUserLogged();
        setAuthedUser(data);
    };

    const onLogout = () => {
        setAuthedUser(null);
        putAccessToken("");
    };

    React.useEffect(() => {
        document.documentElement.setAttribute("data-theme", theme);
    }, [theme]);

    if (initializing) {
        return null;
    }

    if (authedUser === null) {
        return (
            <LocaleProvider value={localeContextValue}>
                <ThemeProvider value={themeContextValue}>
                    <div className="app-container">
                        <Header />
                        <main>
                            <Routes>
                                <Route
                                    path="/*"
                                    element={
                                        <LoginPage
                                            loginSuccess={onLoginSuccess}
                                        />
                                    }
                                />
                                <Route
                                    path="/register"
                                    element={<RegisterPage />}
                                />
                            </Routes>
                        </main>
                    </div>
                </ThemeProvider>
            </LocaleProvider>
        );
    }
    return (
        <LocaleProvider value={localeContextValue}>
            <ThemeProvider value={themeContextValue}>
                <div className="app-container">
                    <Header logout={onLogout} name={authedUser.name} />

                    <main>
                        <Routes>
                            <Route path="/" element={<HomePage />} />
                            <Route
                                path="/archives"
                                element={<ArchivesPage />}
                            />
                            <Route path="/notes/:id" element={<DetailPage />} />
                            <Route path="/notes/new" element={<AddPage />} />
                            <Route path="*" element={<PageNotFound />} />
                        </Routes>
                    </main>
                </div>
            </ThemeProvider>
        </LocaleProvider>
    );
}

export default App;
